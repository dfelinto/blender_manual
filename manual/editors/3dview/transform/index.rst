
############
  Transform
############

.. toctree::
   :maxdepth: 2

   introduction.rst
   grab.rst
   rotate.rst
   scale.rst
   duplication/index.rst
   origns.rst
   transform_control/index.rst
   tools.rst
