
*****
Group
*****

These are tools used group multiple nodes into one and are documented
:doc:`Here </editors/node_editor/node_groups>`.
